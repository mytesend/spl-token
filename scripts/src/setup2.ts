import {
  Connection,
  LAMPORTS_PER_SOL,
  PublicKey,
  Signer,
} from "@solana/web3.js";

import { Token, TOKEN_PROGRAM_ID } from "@solana/spl-token";
import {
  getKeypair,
  getAccountKeypair,
  getAccountPublicKey,
  getTokenBalance,
  getBalance,
  writePublicKey,
} from "./utils";

const createMint = (
  connection: Connection,
  { publicKey, secretKey }: Signer
) => {
  return Token.createMint(
    connection,
    {
      publicKey,
      secretKey,
    },
    publicKey,
    null,
    0,
    TOKEN_PROGRAM_ID
  );
};


const setupMintHanaToken = async (
  name: string,
  connection: Connection,
  lamPublicKey: PublicKey,
  hanPublicKey: PublicKey,
  relayerPublicKey: PublicKey,
  clientKeypair: Signer
): Promise<[Token, PublicKey, PublicKey, PublicKey]> => {
  console.log(`Creating Mint ${name}-Token...`);
  //Create SPL Token L
  const mint = await createMint(connection, clientKeypair);
  writePublicKey(mint.publicKey, `mint_${name.toLowerCase()}`, "/mint");
  console.log(`mint ${name}-Token : `, mint.publicKey.toString());


  console.log(`Creating Owner(relayer) SPLToken-Account for ${name}-Token... ${mint.publicKey} `);
  const relayerTokenAccount = await mint.createAccount(relayerPublicKey);
  writePublicKey(relayerTokenAccount, `owner_${name.toLowerCase()}`, "/account");
  console.log(`SPLToken-Account of Relayer is ${relayerTokenAccount.toString()}`);


  console.log(`Creating Lam SPLToken-Account for ${name}-Token... ${mint.publicKey} `);
  const lamTokenAccount = await mint.createAccount(lamPublicKey);
  writePublicKey(lamTokenAccount, `lam_${name.toLowerCase()}`, "/account");
  console.log(`SPLToken-Account of Lam is ${lamTokenAccount.toString()}`);

  console.log(`Creating Han SPLToken-Account for ${name}-Token... ${mint.publicKey}`);
  const hanTokenAccount = await mint.createAccount(hanPublicKey);
  writePublicKey(hanTokenAccount, `han_${name.toLowerCase()}`, "/account");
  console.log(`SPLToken-Account of Han is ${hanTokenAccount.toString()}`);

  return [mint, lamTokenAccount, hanTokenAccount, relayerTokenAccount];
};


const setupNew = async () => {
  const relayerPublicKey = getAccountPublicKey("owner");
  const lamPublicKey = getAccountPublicKey("lam");
  const hanPublicKey = getAccountPublicKey("han");
  const clientKeypair = getAccountKeypair("owner");

  const connection = new Connection("http://localhost:8899", "confirmed");

  console.log("Requesting SOL for Owner Account a.k.a Relayer ...");
  await connection.requestAirdrop(relayerPublicKey, LAMPORTS_PER_SOL * 15);
  

  console.log("Setting up account with SPL-Token named Hana token ...");
 
  const [mintL, lamTokenAccountForL, hanTokenAccountForL, relayerTokenAccountForL] = await setupMintHanaToken(
    "Hana",
    connection,
    lamPublicKey,
    hanPublicKey,
    relayerPublicKey,
    clientKeypair
  );

  console.log("Sending 100 Hanas to Lam's Hana-Token-Account...");
  await mintL.mintTo(lamTokenAccountForL, clientKeypair.publicKey, [], 100);
  console.log("✨Setup complete✨\n");
  console.table([
    {
      "Relayer SOL Account ": await getBalance(
        relayerPublicKey,
        connection
      ) /  LAMPORTS_PER_SOL,
      "Relayer Hana-Token-Account ": await getTokenBalance(
        relayerTokenAccountForL,
        connection
      ),
      "Lam Hana-Token-Account ": await getTokenBalance(
        lamTokenAccountForL,
        connection
      ),
      "Han Hana-Token-Account": await getTokenBalance(
        hanTokenAccountForL,
        connection
      ),
    },
  ]);
  console.log("");
};


setupNew();