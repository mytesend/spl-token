import { AccountLayout, Token, TOKEN_PROGRAM_ID } from "@solana/spl-token";
import {
  Connection,
  Account,
  Keypair,
  PublicKey,
  SystemProgram,
  BpfLoader,
  SYSVAR_RENT_PUBKEY,
  Transaction,
  TransactionInstruction,
  LAMPORTS_PER_SOL,
  BPF_LOADER_DEPRECATED_PROGRAM_ID
} from "@solana/web3.js";
import BN = require("bn.js");
import {

  getPublicKey,
  getAccountKeypair,
  getAccountPublicKey,
  getTokenBalance,
  getBalance,
  logError,
  getOwnerProgramId
} from "./utils";


const run = async () => {
  
  //const terms = getTerms();

  const LamHanaTokenAccountPubkey = getAccountPublicKey("lam_hana");
  const HanHanaTokenAccountPubkey = getAccountPublicKey("han_hana");
  const OwnerHanaTokenAccountPubkey = getAccountPublicKey("owner_hana");
  const HanaTokenMintPubkey = getPublicKey("mint_hana", "/mint");
  const ownerAccountPubkey = getAccountPublicKey("owner");
  const LamAccountPubkey = getAccountPublicKey("lam");
  const HanAccountPubkey = getAccountPublicKey("han");
  const ownerKeypair = getAccountKeypair("owner");
  const lamKeypair = getAccountKeypair("lam");

  const connection = new Connection("http://localhost:8899");

  
  let programId = getOwnerProgramId();
  console.log("Program ID : ", programId);

  
  const initSmartContractTransactionIx = new TransactionInstruction({
    programId: programId,
    keys: [
     // { pubkey: new PublicKey('11111111111111111111111111111111'), isSigner: false, isWritable: true },
      { pubkey: ownerKeypair.publicKey, isSigner: true, isWritable: false },
      { pubkey: LamAccountPubkey, isSigner: false, isWritable: true },
      { pubkey: HanAccountPubkey, isSigner: false, isWritable: false },
      {
        pubkey: OwnerHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: true,
      },
      {
        pubkey: HanHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: false,
      },
      {
        pubkey: LamHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: false,
      },
      { pubkey: TOKEN_PROGRAM_ID, isSigner: false, isWritable: false },
      { pubkey: programId, isSigner: false, isWritable: false },

    ],
    
    data: Buffer.from(
      Uint8Array.of(2, ...new BN(40000).toArray("le", 8))
    ),
  });

  const tx = new Transaction({feePayer: ownerKeypair.publicKey}).add(
    initSmartContractTransactionIx
  );
  await connection.sendTransaction(
    tx,
    [ownerKeypair],
    { skipPreflight: false, preflightCommitment: "confirmed" }
  );

  // sleep to allow time to update
  await new Promise((resolve) => setTimeout(resolve, 1000));

  console.log(`Owner account ${ownerAccountPubkey.toBase58()}`);
  
  console.log(`Lam account ${getAccountPublicKey("lam").toBase58()}`);
 

  console.log("");
};

run();
