import { AccountLayout, Token, TOKEN_PROGRAM_ID } from "@solana/spl-token";
import {
  Connection,
  Account,
  Keypair,
  PublicKey,
  SystemProgram,
  BpfLoader,
  SYSVAR_RENT_PUBKEY,
  Transaction,
  TransactionInstruction,
  LAMPORTS_PER_SOL,
  BPF_LOADER_DEPRECATED_PROGRAM_ID
} from "@solana/web3.js";
import BN = require("bn.js");
import {
  EscrowLayout,
  ESCROW_ACCOUNT_DATA_LAYOUT,
  getKeypair,
  getProgramId,
  createAndGetProgramId,
  getPublicKey,
  getAccountKeypair,
  getAccountPublicKey,
  //getTerms,
  getTokenBalance,
  getBalance,
  logError,
  writePublicKey,
  getOwnerProgramId
} from "./utils";
import fs from 'mz/fs';


const run = async () => {
  
  //const terms = getTerms();

  const LamHanaTokenAccountPubkey = getAccountPublicKey("lam_hana");
  const HanHanaTokenAccountPubkey = getAccountPublicKey("han_hana");
  const OwnerHanaTokenAccountPubkey = getAccountPublicKey("owner_hana");
  const HanaTokenMintPubkey = getPublicKey("mint_hana", "/mint");
  const ownerAccountPubkey = getAccountPublicKey("owner");
  const LamAccountPubkey = getAccountPublicKey("lam");
  const HanAccountPubkey = getAccountPublicKey("han");
  const ownerKeypair = getAccountKeypair("owner");
  const lamKeypair = getAccountKeypair("lam");
  const dangKeypair = getAccountKeypair("dang");
  //const transferPublicKey = new PublicKey('11111111111111111111111111111111');

  

  const connection = new Connection("http://localhost:8899");

  // /** Test cho nay */
  // programAccount = new Account();
  // let programId;
  // let payerAccount = new Account([3,23,7,78,16,58,170,125,202,176,39,8,167,255,132,83,197,24,218,128,160,72,41,245,187,11,20,99,195,120,110,8,4,197,93,135,109,93,242,101,160,191,97,95,48,122,235,14,101,236,136,15,245,141,2,181,133,99,6,118,94,47,203,28]);
  // const data = await fs.readFile("../dist/program/solana_escrow.so");
  // console.log("data : ", data);
  // await BpfLoader.load(
  //       connection,
  //       payerAccount,
  //       programAccount,
  //       data,
  //       BPF_LOADER_DEPRECATED_PROGRAM_ID,
  //   );

  // programId = programAccount.publicKey;

  // /** End test */

  // //const programId = await createAndGetProgramId(ownerKeypair, connection);
  let programId = getOwnerProgramId();
    
  console.log("Program ID : ", programId);
  let amountHanaTokenTransfer = 4;

  //CAI NAY GOI VAO TOKEN PROGRAM MAC DINH CUA SOLANA, KO CO GOI VAO SMART CONTRACT nen tam thoi ko xai
  const transferHanaTokensFromLamToHan = Token.createTransferInstruction(
    TOKEN_PROGRAM_ID,
    LamHanaTokenAccountPubkey,
    HanHanaTokenAccountPubkey,
    ownerKeypair.publicKey,
    [],
    amountHanaTokenTransfer
  );
  
  const initSmartContractTransactionIx = new TransactionInstruction({
    programId: programId,
    keys: [
      // { pubkey: transferPublicKey, isSigner: false, isWritable: true },
      { pubkey: ownerKeypair.publicKey, isSigner: true, isWritable: false },
      { pubkey: LamAccountPubkey, isSigner: false, isWritable: true },
      { pubkey: HanAccountPubkey, isSigner: false, isWritable: false },
      {
        pubkey: OwnerHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: true,
      },
      {
        pubkey: HanHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: false,
      },
      {
        pubkey: LamHanaTokenAccountPubkey,
        isSigner: false,
        isWritable: false,
      },
      { pubkey: TOKEN_PROGRAM_ID, isSigner: false, isWritable: false },
      { pubkey: programId, isSigner: false, isWritable: false },

      { pubkey: dangKeypair.publicKey, isSigner: false, isWritable: false },
    ],
    
    data: Buffer.from(
      Uint8Array.of(2, ...new BN(amountHanaTokenTransfer).toArray("le", 8))
    ),
  });

  const tx = new Transaction({feePayer: ownerKeypair.publicKey}).add(
  //  transferHanaTokensFromLamToHan,
    initSmartContractTransactionIx
  );
  console.log("Sending Relayer's transaction with token ..." , HanaTokenMintPubkey.toBase58());
  await connection.sendTransaction(
    tx,
    [ownerKeypair],
    { skipPreflight: false, preflightCommitment: "confirmed" }
  );

  // sleep to allow time to update
  await new Promise((resolve) => setTimeout(resolve, 1000));

  const relayerAccount = await connection.getAccountInfo(
    ownerKeypair.publicKey
  );
  console.log(relayerAccount);
  if (relayerAccount === null ){
  //  || relayerAccount.data.length === 0) {
    logError(`Relayer state account failed with pubkey ${ownerKeypair.publicKey}`);
    process.exit(1);
  }

  
  console.log(
    `✨Transfer successfully. Lam is transfer ${amountHanaTokenTransfer} Hana to Han✨\n`
  );
 
  console.log(`Owner account ${ownerAccountPubkey.toBase58()}`);
  console.log(`Owner token-account ${OwnerHanaTokenAccountPubkey.toBase58()}`);
  console.log(`Lam account ${getAccountPublicKey("lam").toBase58()}`);
  console.log(`Lam token-account ${LamHanaTokenAccountPubkey.toBase58()}`);
  console.log(`Han account ${getAccountPublicKey("han").toBase58()}`);
  console.log(`Han token-account ${HanHanaTokenAccountPubkey.toBase58()}`);
  console.table([

    {
      "Owner SOL Balance  ":  await getBalance(
        ownerAccountPubkey,
        connection
      ) /  LAMPORTS_PER_SOL,
      "Owner Hana-Token-Account ": await getTokenBalance(
        OwnerHanaTokenAccountPubkey,
        connection
      ),
      "Lam Hana-Token-Account ": await getTokenBalance(
        LamHanaTokenAccountPubkey,
        connection
      ),
      "Han Hana-Token-Account": await getTokenBalance(
        HanHanaTokenAccountPubkey,
        connection
      ),
    },
  ]);

  console.log("");
};

run();
