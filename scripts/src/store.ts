/**
 * Simple file-based datastore
 *
 * @flow
 */

import * as path from 'path';
import * as fs from 'mz/fs';
import mkdirp from "mkdirp-promise";
import {
  Account,
  Connection,
  BpfLoader,
  BPF_LOADER_DEPRECATED_PROGRAM_ID,
  Signer
  
} from '@solana/web3.js';

const dir = path.join(__dirname, '../../../store');
 export class Store {
  dir = path.join(__dirname, '../../../store');

  async load(uri: string): Promise<Object> {
    const filename = path.join(this.dir, uri);
    const data = await fs.readFile(filename, 'utf8');
    const config = JSON.parse(data);
    return config;
  }

  async save(uri: string, config: Object): Promise<void> {
    await mkdirp(this.dir);
    const filename = path.join(this.dir, uri);
    await fs.writeFile(filename, JSON.stringify(config), 'utf8');
  }
}

async function load(uri: string) {
  const filename = path.join(dir, uri);
  const data = await fs.readFile(filename, 'utf8');
  const config = JSON.parse(data);
  return config;
}

async function save(uri: string, config: Object): Promise<void> {
  await mkdirp(dir);
  const filename = path.join(dir, uri);
  await fs.writeFile(filename, JSON.stringify(config), 'utf8');
}

export async function loadProgram(payerAccount: Account|Signer, connection: Connection){
  // Load the program
  console.log('\nLoading program...\n');
  let pathToProgram = "../dist/program/solana_escrow.so";
  const data = await fs.readFile(pathToProgram);
  let programAccount = new Account();

  await BpfLoader.load(
      connection,
      payerAccount,
      programAccount,
      data,
      BPF_LOADER_DEPRECATED_PROGRAM_ID,
  );

  let programId = programAccount.publicKey;
  console.log('Program loaded to account', programId.toBase58());

  // Save this info for next time
    save('../terms.json', {
      "aliceExpectedAmount": 3,
      "bobExpectedAmount": 5,
      "programId": programId.toBase58()
    }
    );
  return programId;
}