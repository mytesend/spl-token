use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint::ProgramResult,
    msg,
    program::{invoke, invoke_signed},
    program_error::ProgramError,
    program_pack::{IsInitialized, Pack},
    pubkey::Pubkey,
    sysvar::{rent::Rent, Sysvar},
    system_instruction
};

use spl_token::state::Account as TokenAccount;

use crate::{error::EscrowError, instruction::SmartContractInstruction, state::Escrow};

pub struct Processor;
impl Processor {
    pub fn process(
        program_id: &Pubkey,
        accounts: &[AccountInfo],
        instruction_data: &[u8],
    ) -> ProgramResult {
        let instruction = SmartContractInstruction::unpack(instruction_data)?;

        match instruction {
            SmartContractInstruction::InitEscrow { amount } => {
                msg!("Instruction: InitEscrow nè test 4");
                Self::process_init_escrow(accounts, amount, program_id)
            }
            SmartContractInstruction::Exchange { amount } => {
                msg!("Instruction: Exchange test 1");
                Self::process_exchange(accounts, amount, program_id)
            }
            SmartContractInstruction::TransferToken { amount } => {
                msg!("Instruction: Transfer nè");
                Self::process_transfer(accounts, amount, program_id)
            }
        }
    }

    fn process_init_escrow(
        accounts: &[AccountInfo],
        amount: u64,
        program_id: &Pubkey,
    ) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let initializer = next_account_info(account_info_iter)?;

        if !initializer.is_signer {
            return Err(ProgramError::MissingRequiredSignature);
        }

        let temp_token_account = next_account_info(account_info_iter)?;

        let token_to_receive_account = next_account_info(account_info_iter)?;
        if *token_to_receive_account.owner != spl_token::id() {
            return Err(ProgramError::IncorrectProgramId);
        }

        let escrow_account = next_account_info(account_info_iter)?;
        let rent = &Rent::from_account_info(next_account_info(account_info_iter)?)?;

        if !rent.is_exempt(escrow_account.lamports(), escrow_account.data_len()) {
            return Err(EscrowError::NotRentExempt.into());
        }

        let mut escrow_info = Escrow::unpack_unchecked(&escrow_account.try_borrow_data()?)?;
        if escrow_info.is_initialized() {
            return Err(ProgramError::AccountAlreadyInitialized);
        }

        escrow_info.is_initialized = true;
        escrow_info.initializer_pubkey = *initializer.key;
        escrow_info.temp_token_account_pubkey = *temp_token_account.key;
        escrow_info.initializer_token_to_receive_account_pubkey = *token_to_receive_account.key;
        escrow_info.expected_amount = amount;

        Escrow::pack(escrow_info, &mut escrow_account.try_borrow_mut_data()?)?;
        let (pda, _nonce) = Pubkey::find_program_address(&[b"escrow"], program_id);

        let token_program = next_account_info(account_info_iter)?;
        let owner_change_ix = spl_token::instruction::set_authority(
            token_program.key,
            temp_token_account.key,
            Some(&pda),
            spl_token::instruction::AuthorityType::AccountOwner,
            initializer.key,
            &[&initializer.key],
        )?;

        msg!("Calling the token program to transfer token account ownership...");
        invoke(
            &owner_change_ix,
            &[
                temp_token_account.clone(),
                initializer.clone(),
                token_program.clone(),
            ],
        )?;

        Ok(())
    }

    fn process_exchange(
        accounts: &[AccountInfo],
        amount_expected_by_taker: u64,
        program_id: &Pubkey,
    ) -> ProgramResult {
        let account_info_iter = &mut accounts.iter();
        let taker = next_account_info(account_info_iter)?;

        if !taker.is_signer {
            return Err(ProgramError::MissingRequiredSignature);
        }

        let takers_sending_token_account = next_account_info(account_info_iter)?;

        let takers_token_to_receive_account = next_account_info(account_info_iter)?;

        let pdas_temp_token_account = next_account_info(account_info_iter)?;
        let pdas_temp_token_account_info =
            TokenAccount::unpack(&pdas_temp_token_account.try_borrow_data()?)?;
        let (pda, nonce) = Pubkey::find_program_address(&[b"escrow"], program_id);

        if amount_expected_by_taker != pdas_temp_token_account_info.amount {
            return Err(EscrowError::ExpectedAmountMismatch.into());
        }

        let initializers_main_account = next_account_info(account_info_iter)?;
        let initializers_token_to_receive_account = next_account_info(account_info_iter)?;
        let escrow_account = next_account_info(account_info_iter)?;

        let escrow_info = Escrow::unpack(&escrow_account.try_borrow_data()?)?;

        if escrow_info.temp_token_account_pubkey != *pdas_temp_token_account.key {
            return Err(ProgramError::InvalidAccountData);
        }

        if escrow_info.initializer_pubkey != *initializers_main_account.key {
            return Err(ProgramError::InvalidAccountData);
        }

        if escrow_info.initializer_token_to_receive_account_pubkey
            != *initializers_token_to_receive_account.key
        {
            return Err(ProgramError::InvalidAccountData);
        }

        let token_program = next_account_info(account_info_iter)?;

        let transfer_to_initializer_ix = spl_token::instruction::transfer(
            token_program.key,
            takers_sending_token_account.key,
            initializers_token_to_receive_account.key,
            taker.key,
            &[&taker.key],
            escrow_info.expected_amount,
        )?;
        msg!("Calling the token program to transfer tokens to the escrow's initializer...");
        invoke(
            &transfer_to_initializer_ix,
            &[
                takers_sending_token_account.clone(),
                initializers_token_to_receive_account.clone(),
                taker.clone(),
                token_program.clone(),
            ],
        )?;

        let pda_account = next_account_info(account_info_iter)?;

        let transfer_to_taker_ix = spl_token::instruction::transfer(
            token_program.key,
            pdas_temp_token_account.key,
            takers_token_to_receive_account.key,
            &pda,
            &[&pda],
            pdas_temp_token_account_info.amount,
        )?;
        msg!("Calling the token program to transfer tokens to the taker...");
        invoke_signed(
            &transfer_to_taker_ix,
            &[
                pdas_temp_token_account.clone(),
                takers_token_to_receive_account.clone(),
                pda_account.clone(),
                token_program.clone(),
            ],
            &[&[&b"escrow"[..], &[nonce]]],
        )?;

        let close_pdas_temp_acc_ix = spl_token::instruction::close_account(
            token_program.key,
            pdas_temp_token_account.key,
            initializers_main_account.key,
            &pda,
            &[&pda],
        )?;
        msg!("Calling the token program to close pda's temp account...");
        invoke_signed(
            &close_pdas_temp_acc_ix,
            &[
                pdas_temp_token_account.clone(),
                initializers_main_account.clone(),
                pda_account.clone(),
                token_program.clone(),
            ],
            &[&[&b"escrow"[..], &[nonce]]],
        )?;

        msg!("Closing the escrow account...");
        **initializers_main_account.try_borrow_mut_lamports()? = initializers_main_account
            .lamports()
            .checked_add(escrow_account.lamports())
            .ok_or(EscrowError::AmountOverflow)?;
        **escrow_account.try_borrow_mut_lamports()? = 0;
        *escrow_account.try_borrow_mut_data()? = &mut [];

        Ok(())
    }




    fn process_transfer(
        accounts: &[AccountInfo],
        amount_expected: u64,
        program_id: &Pubkey,
    ) -> ProgramResult {
        msg!("Bắt đầu process transfer...");
        let account_info_iter = &mut accounts.iter();
        //let target_program_info = next_account_info(account_info_iter)?;
        let owner_account = next_account_info(account_info_iter)?;
        let from_account = next_account_info(account_info_iter)?;
        let to_account = next_account_info(account_info_iter)?;
        let owner_token_account = next_account_info(account_info_iter)?;
        let to_token_account = next_account_info(account_info_iter)?;
        let from_token_account = next_account_info(account_info_iter)?;

        let token_program = next_account_info(account_info_iter)?;

        msg!("Calling aaaathe token program to transfer tokens from Lam to the owner...");
        msg!("Amount can transfer la {}", amount_expected);
        msg!("owner_account {:?}", owner_account);
        msg!("owner_token_account {:?}", owner_token_account.key);
        msg!("from_account (lam) {:?}", from_account);
        msg!("from_token_account {:?}", from_token_account);
        msg!("to_account (han) {:?}", to_account);
        msg!("to_token_account {:?}", to_token_account.key);
    
        msg!("token_program {:?}", token_program);

        
        let current_program = next_account_info(account_info_iter)?;

      

      //  msg!("current_program {:?}", current_program);
      /* 
        let inst = system_instruction::transfer(&owner_account.key, &from_account.key, 800000000);
        match invoke(
            &inst,
            &[
                owner_account.clone(),
                to_account.clone(),
                target_program_info.clone()
                //current_program.clone(),
            ],
        ){
            Ok(_) => {msg!("Match OK ");},
            Err(error) => {
                msg!("error here ....{:?}", error);
                return Err(error);
            }
        };   

        */
        msg!("start new test ");
        

        let transfer_from_from_to_owner = spl_token::instruction::transfer(
            token_program.key,
            from_token_account.key,
            owner_token_account.key,
            owner_account.key,
            &[&owner_account.key],
            amount_expected,
        )?;

        invoke(
            &transfer_from_from_to_owner,
            &[
                from_token_account.clone(),
                owner_token_account.clone(),
                owner_account.clone(),
                token_program.clone(),
            ],
        )?;


        let new_transfer_amount = ((amount_expected.clone()) / 100)  as u64;
        let transfer_from_owner_to_to = spl_token::instruction::transfer(
            token_program.key,
            owner_token_account.key,
            to_token_account.key,
            owner_account.key,
            &[&owner_account.key],
            new_transfer_amount,
        )?;

        invoke(
            &transfer_from_owner_to_to,
            &[
                owner_token_account.clone(),
                to_token_account.clone(),
                owner_account.clone(),
                token_program.clone(),
            ],
        )?;

        
        msg!("test ok");
        Ok(())
    }
}
